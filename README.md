# vue-test

> Tiny VueJS project for really basic blog.
>
> Main objective of this project is to test async themeable component loading with fallback to (configurable) default theme
>
> Currently it has no tests (Just some defaults that are outdated. Should add real tests one day perhaps..)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
