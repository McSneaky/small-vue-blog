/**
 * Component loader class
 *
 * @class      ComponentLoader
 */
export default class ComponentLoader {

  /**
   * Load component
   *
   * @param      {string}  theme    The theme to load
   * @param      {string}  element  The element to load
   *
   * @return     {promise}
   */
  static load (theme, element) {
    var component = void (0)
    try {
      // Try to load theme component if fails then load fallback component
      component = import('@/themes/' + theme + '/' + element).catch(() => {
        return this.loadFallback(element)
      })
    } catch (e) {
      // If some error happens and module is not found, then load fallback one
      if (e.code === 'MODULE_NOT_FOUND') {
        component = this.loadFallback(element)
      } else {
        // If it is some other error, then thow it back up again
        throw e
      }
    }
    return component
  }

  /**
   * Loads component from fallback theme
   *
   * @param      {string}   element  The element to load
   *
   * @return     {promise}
   */
  static loadFallback (element) {
    return import('@/themes/' + process.env.DEFAULT_THEME + '/' + element)
  }

  static loadTemplete (theme, element) {
    return require('@/themes/' + theme + '/components/Post/' + element)
  }
}

// Would use this approach, but 'this' is not present in components() {}
// const ComponentLoader = {
//   install (Vue, options) {
//     Vue.prototype.$loadComponent = function (theme, element) {
//       var component = void (0)
//       component = import('@/themes/' + theme + '/' + element).catch(() => {
//         return import('@/themes/default/' + element)
//       })
//       return component
//     }
//   }
// }
// export default ComponentLoader
