import axios from 'axios'
import { API_URI } from '@/config'
import store from '@/store'

// Get post view components
import Create from '@/themes/default/components/Post/Create'
import Update from '@/themes/default/components/Post/Update'
import View from '@/themes/default/components/Post/View'
import Posts from '@/themes/default/components/Post/Index'

/**
 * Class for post.
 */
export default class Post {

  /**
   * Constructs the post.
   *
   * @param      {integer}  id      The identifier
   * @param      {string}   title   The title
   * @param      {text}     body    The body
   */
  constructor (id, title, body) {
    this.id = id
    this.title = title
    this.body = body
  }

  /**
   * Save the post
   */
  save () {
    // Check if updating or creating new post
    if (this.id) {
      axios.put(API_URI + 'posts/' + this.id, this).then(({data}) => {
        window.location.href = '/posts/' + data.id
      })
    } else {
      axios.post(API_URI + 'posts', this).then(({data}) => {
        window.location.href = '/thank-you/' + data.id
      })
    }
  }

  /**
   * Delete post from local state and server
   */
  destroy () {
    axios.delete(API_URI + 'posts/' + this.id).then(() => {
      store.dispatch('deletePost', this.id)
    })
  }

  /**
   * Get all posts and store them in local state
   */
  static all () {
    axios.get(API_URI + 'posts').then(({data}) => {
      store.dispatch('setPosts', data)
    })
  }

  /**
   * Get one post and store it to local state
   *
   * @param      {integer|string}  id      the post id
   */
  static one (id) {
    axios.get(API_URI + 'posts/' + id).then(({data}) => {
      store.dispatch('openPost', new Post(data.id, data.title, data.body))
    })
  }
}

// CRU(D) routes that are used by Post
export const PostRoutes = [
  {
    path: '/posts',
    name: 'Posts',
    component: Posts
  },
  {
    path: '/posts/create',
    name: 'PostsCreate',
    component: Create
  },
  {
    path: '/posts/edit/:id',
    name: 'PostsUpdate',
    component: Update
  },
  {
    path: '/posts/:id',
    name: 'PostsView',
    component: View
  }
]
