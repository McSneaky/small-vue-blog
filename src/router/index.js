import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/themes/default/components/Home'
import Test from '@/themes/default/components/Test'
import ThankYou from '@/themes/default/components/Post/ThankYou'
import {PostRoutes} from '@/models/Post'

Vue.use(Router)

// General routes
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/test',
    name: 'Test',
    component: Test
  },
  {
    path: '/thank-you/:id',
    name: 'ThankYou',
    component: ThankYou
  }
]

export default new Router({
  // Concat routes with Post routes
  routes: routes.concat(PostRoutes),

  mode: 'history'
})
