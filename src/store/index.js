import Vue from 'vue'
import Vuex from 'vuex'
import Post from '@/models/Post'

Vue.use(Vuex)

// Variables to keep in local state
const state = {
  post: {},
  posts: []
}

// Actions that are called thoughout app that that change state Actually can
// call mutations directly too but calling actions is much nicer and better to
// track. Just harder to shoot yourself to leg, if using actions
const actions = {

  /**
   * Opens a post.
   *
   * @param      {Object}         arg1         store
   * @param      {Function}       arg1.commit  commit()
   * @param      {@/models/Post}  post         The post to open
   */
  openPost ({commit}, post) {
    commit('OPEN_POST', post)
  },

  /**
   * Closes opened post.
   *
   * @param      {Object}    arg1           store
   * @param      {Function}  arg1.dispatch  dispatch()
   */
  closePost ({dispatch}) {
    dispatch('openPost', void (0))
  },

  /**
   * Sets the posts.
   *
   * @param      {Object}           arg1         store
   * @param      {Function}         arg1.commit  commit()
   * @param      {@/models/Post[]}  posts        The posts
   */
  setPosts ({commit}, posts) {
    // First clear all old posts
    commit('CLEAR_ALL_POSTS')
    var len = posts.length
    for (var i = 0; i < len; i++) {
      // Then create new Post objects and store them in local state
      let post = new Post(
        posts[i].id,
        posts[i].title,
        posts[i].body
      )
      commit('ADD_POST', post)
    }
  },

  /**
   * Deletes the post
   *
   * @param      {Object}    arg1         store
   * @param      {Function}  arg1.commit  commit()
   * @param      {integer}   id           The post identifier
   */
  deletePost ({commit}, id) {
    commit('DELETE_POST', id)
  }
}

// Mutations that change local state
const mutations = {

  /**
   * Sets 'post'
   *
   * @param      {Object}         state   Current local state
   * @param      {@/models/Post}  post    The post
   */
  OPEN_POST (state, post) {
    state.post = post
  },

  /**
   * Adds post to 'posts' array.
   *
   * @param      {Object}         state   Current local state
   * @param      {@/models/Post}  post    The post
   */
  ADD_POST (state, post) {
    state.posts.push(post)
  },

  /**
   * Deletes post from 'posts' array
   *
   * @param      {Object}   state   Current local state
   * @param      {integer}  id      The post identifier
   */
  DELETE_POST (state, id) {
    var len = state.posts.length
    for (var i = 0; i < len; i++) {
      if (state.posts[i].id === id) {
        state.posts.splice(i, 1)
        break
      }
    }
  },

  /**
   * Clears 'posts' array
   *
   * @param      {Object}  state   Current local state
   */
  CLEAR_ALL_POSTS (state) {
    state.posts = []
  }
}

export default new Vuex.Store({
  state,
  actions,
  mutations
})
